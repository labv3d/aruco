#include "opencv2/aruco/dictionary.hpp"
#include "opencv2/aruco.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgcodecs.hpp"
#include <string>
#include <iostream>
#include <sys/time.h>

#include <dirent.h>

using namespace std;
using namespace cv;

std::vector<int> worldIds;
std::vector<cv::Mat> worldPoses;
std::vector<cv::Mat> worldCam; // world to cam , each step

//#include  "opencv2/xfeatures2d.hpp" 

/*
void test()  {
    Ptr< cv::xfeatures2d::SURF> surf =  xfeatures2d::SURF::create();
}
*/

void dump4x4(Mat m) {
    cout    << " " << m.at<double>(0,0)
            << " " << m.at<double>(0,1)
            << " " << m.at<double>(0,2)
            << " " << m.at<double>(0,3)
            << " " << m.at<double>(1,0)
            << " " << m.at<double>(1,1)
            << " " << m.at<double>(1,2)
            << " " << m.at<double>(1,3)
            << " " << m.at<double>(2,0)
            << " " << m.at<double>(2,1)
            << " " << m.at<double>(2,2)
            << " " << m.at<double>(2,3)
            << " " << m.at<double>(3,0)
            << " " << m.at<double>(3,1)
            << " " << m.at<double>(3,2)
            << " " << m.at<double>(3,3) << "\n";
}

void trajectoryDump() {
    for(int i=0;i<worldIds.size();i++) {
        cout << "@ "<<i<<" "<< worldIds[i];
        dump4x4(worldPoses[i]);
    }
    for(int i=0;i<worldCam.size();i++) {
        cout << "@@ "<<i<<" -1 ";
        dump4x4(worldCam[i]);
    }
}

/////////////////////////////////////
//
// managing aruco positions and camera positions
//
// worldId[i] = "id" of a marker
// worldPose[i] = pose of marker (4x4,RT=rot+trans, RT.[marker ref]->[pos in the camera system. Ex: RT.{0,0,0} -> marker corner in the camera world
// cameraPose[n] = camera pose at time t.
// 
// adding 1 marker: only if id does not exist
// adding 2 markers : if marker i known, j unkown, then add RT_j.RT_i^-1*RT[i]
//
////
void trajectory(std::vector<int> ids, std::vector<Mat> poses) {
    cout << "-- poses " << ids.size() << "--\n";
    if( worldIds.size()==0 ) {
        // le premier tag est aligne avec le monde
        worldIds.push_back(ids[0]); // premier tag vu
        Mat ident= Mat::zeros(4, 4, CV_64F);
        for(int i=0;i<4;i++) ident.at<double>(i,i)=1.0;
        worldPoses.push_back(ident);
        cout << "init pose tag "<<ids[0]<<" identity\n";
    }
    // premier ID qu'on trouve qui est connu, on obtient la cam pos
    // on push 2x la position si on a 2 tags...
    for(int i=0;i<worldIds.size();i++) {
        for(int j=0;j<ids.size();j++) {
            if( worldIds[i]==ids[j] ) {
                cout << "match! id="<<ids[j]<<"\n";
                //cout << "tag pose is "<<worldPoses[i]<<"\n";
                //cout << "cam pose is "<<poses[j]<<"\n";
                Mat ppp=poses[j]*worldPoses[i].inv();
                worldCam.push_back(ppp);
                //cout << "cam pose is "<<ppp<<"\n";
            }
        }
    }
    // trouve un ID qui est defini parmis les ids
    int iref=-1;
    for(int i=0;i<worldIds.size();i++) {
        for(int j=0;j<ids.size();j++) {
            if( worldIds[i]==ids[j] ) iref=i; // reference!
        }
    }
    if( iref<0 ) { printf("blub...\n");exit(0); }
    // ajuste la position des tags qui n'ont pas de pose
    for(int j=0;j<ids.size();j++) {
        int i;
        for(i=0;i<worldIds.size();i++) if( worldIds[i]==ids[j] ) break;
        if( i<worldIds.size() ) continue;
        // we need to define j
        Mat ppp=poses[j]*worldPoses[iref];
        cout << "tag "<<ids[j]<<" pose is "<<ppp<<"\n";
        worldIds.push_back(ids[j]); // nouvel id
        worldPoses.push_back(ppp);
    }

}

/////////////////////////////////////

cv::Mat interne;

// les homographies accumulees jusqu'ici
//std::vector<cv::Mat> allH;
std::vector<std::vector<cv::Vec3f>> calibPointsRef;
std::vector<std::vector<cv::Vec2f>> calibPointsImg;
int width,height; // image dimensions

void calibreAccumule(std::vector<cv::Point_<float>> pts) {
    cout << "Accumule! " << pts[0] << "\n";
    //allH.push_back(h);
    std::vector<cv::Vec3f> refp(4);
	refp[0]=cv::Vec3f(0.0,0.0,0.0);
	refp[1]=cv::Vec3f(0.0,10.0,0.0);
	refp[2]=cv::Vec3f(10.0,10.0,0.0);
	refp[3]=cv::Vec3f(10.0,0.0,0.0);
    calibPointsRef.push_back(refp);
    std::vector<cv::Vec2f> p(4);
    for(int i=0;i<pts.size();i++) p[i]=cv::Vec2f(pts[i].x,pts[i].y);
    calibPointsImg.push_back(p);
    //cout << "ref  " << markerCorners[0][coin].x << "  "  << markerCorners[0][coin].y << "\n";
}

void calibreDo() {
    cout << "NB POINTS " <<calibPointsRef.size() << "\n";
    for(int i=0;i<calibPointsRef.size();i++) {
        printf("--- %d ---\n",i);
        for(int j=0;j<calibPointsRef[i].size();j++) {
            cout << "  " << calibPointsImg[i][j] << " === " << calibPointsRef[i][j] <<"\n";
        }
    }

    interne = Mat::eye(3, 3, CV_64F);
    interne.at<double>(0,0) = 1.0; // aspect ratio fixed
    interne.at<double>(1,1) = 1.0; // aspect ratio fixed

    cv::Mat distCoeffs = Mat::zeros(8, 1, CV_64F); // init at 0

    std::vector<cv::Mat> rvecs, tvecs;
    double err;

    err=cv::calibrateCamera(calibPointsRef,
		calibPointsImg,
		cv::Size(width,height),
		interne,
		distCoeffs,
		rvecs,
		tvecs,
		CALIB_FIX_ASPECT_RATIO| CALIB_ZERO_TANGENT_DIST | CALIB_FIX_K1 | CALIB_FIX_K2 | CALIB_FIX_K3 | CALIB_FIX_K4 | CALIB_FIX_K5 | CALIB_FIX_K6,
		TermCriteria(TermCriteria::COUNT+TermCriteria::EPS,30,DBL_EPSILON)
	);
    cout << "err "<< err <<"\n";
    cout << "K " << interne << "\n";
    cout << "dist " << distCoeffs << "\n";
}



////////////////////////////////////

static void drawQRCodeContour(Mat &color_image, vector<Point> transform);
static void drawFPS(Mat &color_image, double fps);
static int  liveQRCodeDetect(const string& out_file);
static int  imageQRCodeDetect(const string& in_file, const string& out_file);

std::vector<int> markerIds;
std::vector<std::vector<cv::Point2f>> markerCorners;
std::vector<std::vector<cv::Point2f>> rejectedCandidates;
std::vector<cv::Mat> markerPoses;

std::vector<cv::Point2f> referenceCorners(4);

cv::Ptr<cv::aruco::DetectorParameters> parameters;
cv::Ptr<cv::aruco::Dictionary> dictionary;

std::vector<cv::Mat> sequence;
std::vector<double> timestamp;

double horloge()
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return((double)tv.tv_sec + tv.tv_usec/1000000.0);
}

// retourne la pose (RT)
Mat calculePose(std::vector<cv::Point2f> ref,std::vector<cv::Point2f> img) {
   // homography such that marker = H reference
   Mat h=cv::findHomography(ref,img);

   //////////

   Mat ki=interne.inv();
   Mat n(3,3,CV_64F);
   n=ki*h;


   /*
   Mat p(3,1,CV_64F);
   p.at<double>(0,0)=referenceCorners[1].x;
   p.at<double>(1,0)=referenceCorners[1].y;
   p.at<double>(2,0)=1.0;

   cout << "REF" << referenceCorners[1] << "\n";
   cout << "MARKER" << markerCorners[0][1] << "\n";
   cout << "P" << p << h*p << "\n";
   p=h*p;
   p.at<double>(0,0)/=p.at<double>(2,0);
   p.at<double>(1,0)/=p.at<double>(2,0);
   p.at<double>(2,0)/=p.at<double>(2,0);
   cout << "P" << p << "\n";
   */

    double r1[3];
    r1[0]=n.at<double>(0,0);
    r1[1]=n.at<double>(1,0);
    r1[2]=n.at<double>(2,0);
    double d1=sqrt(r1[0]*r1[0]+r1[1]*r1[1]+r1[2]*r1[2]);
	r1[0]=r1[0]/d1;
	r1[1]=r1[1]/d1;
	r1[2]=r1[2]/d1;
    //printf("\n\nr1: %12.6f %12.6f %12.6f\n",r1[0],r1[1],r1[2]);

    double r2[3];
    r2[0]=n.at<double>(0,1);
    r2[1]=n.at<double>(1,1);
    r2[2]=n.at<double>(2,1);
    double d2=sqrt(r2[0]*r2[0]+r2[1]*r2[1]+r2[2]*r2[2]);
	r2[0]=r2[0]/d2;
	r2[1]=r2[1]/d2;
	r2[2]=r2[2]/d2;
	//printf("r2: %12.6f %12.6f %12.6f\n",r2[0],r2[1],r2[2]);

    //printf("d1=%12.6f  d2=%12.6f\n",d1,d2);

    double r3[3];  // r3 = r1 X r2
    //{-(x2*y1) + x1*y2, x2*y0 - x0*y2, -(x1*y0) + x0*y1}
    r3[0]=r1[1]*r2[2]-r1[2]*r2[1];
    r3[1]=r1[2]*r2[0]-r1[0]*r2[2];
    r3[2]=r1[0]*r2[1]-r1[1]*r2[0];

	//printf("r3: %12.6f %12.6f %12.6f\n",r3[0],r3[1],r3[2]);

    /// ajuste la rotation
    Mat w, u, vt;
    Mat rot(3,3,CV_64F);
    rot.at<double>(0,0)=r1[0];
    rot.at<double>(1,0)=r1[1];
    rot.at<double>(2,0)=r1[2];
    rot.at<double>(0,1)=r2[0];
    rot.at<double>(1,1)=r2[1];
    rot.at<double>(2,1)=r2[2];
    rot.at<double>(0,2)=r3[0];
    rot.at<double>(1,2)=r3[1];
    rot.at<double>(2,2)=r3[2];
    SVD::compute(rot, w, u, vt);

    //cout << "SVD w = " << w << "\n";

    // ajuste la rotation pour une vraie rotation
    //cout << "avant" << rot  <<"\n";
    rot=u*vt;
    //cout << "apres" << rot  <<"\n";

    // recuper rodrigues
    /* Mat rotv(3,1,CV_64F);
    cv:Rodrigues(rot,rotv);
    printf("r: %12.6f %12.6f %12.6f\n",
            rotv.at<double>(0,0),
            rotv.at<double>(1,0),
            rotv.at<double>(2,0));
            */

    // recupere la translation....
    // la derniere colonne est inv(rot) n3/((d1+d2)/2)
    Mat ni = rot.inv()*n/((d1+d2)/2);
    double t[3];
    t[0]=ni.at<double>(0,2);
    t[1]=ni.at<double>(1,2);
    t[2]=ni.at<double>(2,2);
    printf("t: %12.6f %12.6f %12.6f\n",t[0],t[1],t[2]);

    // Recompose la matrice de projection...
    Mat projk(4,4,CV_64F);
    Mat projr(4,4,CV_64F);
    Mat projt(4,4,CV_64F);
    for(int r=0;r<4;r++) for(int  c=0;c<4;c++) { projk.at<double>(r,c)=0.0; }
    for(int r=0;r<4;r++) for(int  c=0;c<4;c++) { projr.at<double>(r,c)=0.0; }
    for(int r=0;r<4;r++) for(int  c=0;c<4;c++) { projt.at<double>(r,c)=0.0; }
    for(int r=0;r<4;r++)  projk.at<double>(r,r)=1.0;
    for(int r=0;r<4;r++)  projr.at<double>(r,r)=1.0;
    for(int r=0;r<4;r++)  projt.at<double>(r,r)=1.0;
    for(int r=0;r<3;r++) for(int  c=0;c<3;c++) projk.at<double>(r,c)=interne.at<double>(r,c);
    for(int r=0;r<3;r++) for(int  c=0;c<3;c++) projr.at<double>(r,c)=rot.at<double>(r,c);
    for(int r=0;r<3;r++) projt.at<double>(r,3)=t[r];

   //cout << "projK\n" << projk << "\n";
   //cout << "projR\n" << projr << "\n";
   //cout << "projT\n" << projt << "\n";
   //Mat proj=projk*(projr*projt);
   Mat proj=(projr*projt);
   //cout << "proj\n" << proj << "\n";

   // reproj un point pour verifier si ca marche
   /*
   Mat pt(4,1,CV_64F);
   int  coin=0;
   pt.at<double>(0,0)=referenceCorners[coin].x;
   pt.at<double>(1,0)=referenceCorners[coin].y;
   pt.at<double>(2,0)=0.0;
   pt.at<double>(3,0)=1.0;
   Mat ptp=proj*pt;
   ptp.at<double>(0,0)/=ptp.at<double>(2,0);
   ptp.at<double>(1,0)/=ptp.at<double>(2,0);

   cout << "ptp  " << ptp << "\n";
   cout << "ref  " << markerCorners[0][coin].x << "  "  << markerCorners[0][coin].y << "\n";
   cout << "3d   "  << referenceCorners[coin].x << "  "  <<  referenceCorners[coin].y <<  "\n";
   */
   return(proj);
}



void initAruco()
{
	//dictionary= cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);
	dictionary= cv::aruco::getPredefinedDictionary(cv::aruco::DICT_5X5_250);
	//dictionary= cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_100);
	parameters= cv::aruco::DetectorParameters::create();
	parameters->cornerRefinementMethod=cv::aruco::CORNER_REFINE_SUBPIX;
	referenceCorners[0]=cv::Point2f(0.0,0.0);
	referenceCorners[1]=cv::Point2f(0.0,10.0);
	referenceCorners[2]=cv::Point2f(10.0,10.0);
	referenceCorners[3]=cv::Point2f(10.0,0.0);

	interne.create(3, 3, CV_64F);
	for(int i=0;i<3;i++) for(int j=0;j<3;j++) interne.at<double>(i,j)=0.0;
	interne.at<double>(0,0)=705.91; // fx
	interne.at<double>(1,1)=705.91; // fy
	interne.at<double>(0,2)=401.8; // cx
	interne.at<double>(1,2)=296.6; // cy
	interne.at<double>(2,2)=1.0;
}

int doAruco(Mat inputImage)
{
    cv::aruco::detectMarkers(inputImage, dictionary, markerCorners, markerIds, parameters/*, rejectedCandidates*/);
    cv::aruco::drawDetectedMarkers(inputImage, markerCorners, markerIds);

	width=inputImage.size().width;
    height=inputImage.size().height;

    //	cv::Mat outputImage;
   int nb=markerIds.size();
   if( nb>0 ) printf("nb = %3d\n",nb);

   // only one tag for now.
   if( nb==0 ) return(0);

   //markerHomography.resize(nb); // reserve() en vide
   for(int m=0;m<nb;m++) {
       for(int i=0;i<4;i++) {
           printf("%2d : (%12.6f,%12.6f) -> (%12.6f,%12.6f)\n",m,
                   markerCorners[m][i].x,markerCorners[m][i].y,
                   referenceCorners[i].x,referenceCorners[i].y);
       }
   //    markerHomography[m]=cv::findHomography(referenceCorners,markerCorners[m]);
   }

   //  ajusted
   /***
   for(int m=0;m<nb;m++) {
       for(int i=0;i<4;i++) {
           printf("%2d : (%12.6f,%12.6f) -> (%12.6f,%12.6f)\n",m,
                   markerCorners[m][i].x*1024./806,markerCorners[m][i].y*512./605,
                   referenceCorners[i].x,referenceCorners[i].y);
       }
   }
   ****/

   markerPoses.resize(nb);
   for(int i=0;i<nb;i++) {
       markerPoses[i]=calculePose(referenceCorners,markerCorners[i]);
   }

   trajectory(markerIds,markerPoses);

	//printf("(%12.6f,%12.6f)\n",markerCorners[0][0].x,markerCorners[0][1].y);
   return(nb);
}


int main(int argc, char *argv[])
{
double start=horloge();
double last=start;
char videoFileName[200];
char imagesDir[200];

    //
    // si aucun des deux, alors webcam
    //
    videoFileName[0]=0; // pas de video
    imagesDir[0]=0;  //  pas de repertoire  d'images


	for(int i=1;i<argc;i++) {
		if( strcmp(argv[i],"-h")==0 ) {
            printf("aruco [-video toto.mp4] [-images repertoire]\n");
            exit(0);
        }else if( strcmp(argv[i],"-video")==0 && i+1<argc ) {
            strcpy(videoFileName,argv[i+1]);
            i+=1;continue;
        }else if( strcmp(argv[i],"-images")==0 && i+1<argc ) {
            strcpy(imagesDir,argv[i+1]);
            i+=1;
        }
	}


	initAruco();

    DIR *dir=NULL;


    if( imagesDir[0] ) {
        if ((dir = opendir (imagesDir)) != NULL) {
          struct dirent *ent;
          ent = readdir (dir); printf ("SKIP %s\n", ent->d_name);
          ent = readdir (dir); printf ("SKIP %s\n", ent->d_name);
        }else{
            printf("Impossible  d'ouvrir  %s\n",imagesDir);
            exit(0);
        }
    }

    VideoCapture cap;
    if( videoFileName[0] ) {
        cap.open(videoFileName);
        printf("Using  video  file  %s\n",videoFileName);
        if(!cap.isOpened()) { cout << "Cannot open video "<<videoFileName << endl; exit(0); }
    }else if( imagesDir[0]==0 ) {
        cap.open(2);
        printf("Using webcam,,,\n");
        if(!cap.isOpened()) { cout << "Cannot open a camera" << endl; exit(0); }
    }

	//cap.set(CAP_PROP_FRAME_WIDTH,1920);
	//cap.set(CAP_PROP_FRAME_HEIGHT,1080);
	cap.set(CAP_PROP_FRAME_WIDTH,800);
	cap.set(CAP_PROP_FRAME_HEIGHT,600);
	//cap.set(CAP_PROP_AUTOFOCUS, 0);

	Mat frame;
	
	for(int i=0;/*i<1*/;i++) {
        int nb;
		double now=horloge()-start;

        //  images?
        if( dir!=NULL ) {
          struct dirent *ent;
          ent = readdir (dir);
          if( ent==NULL ) break;
          printf("image %s\n",ent->d_name);
          char buf[500];
          sprintf(buf,"%s/%s",imagesDir,ent->d_name);
          printf("read image %s\n",buf);
		  frame=imread(buf, -1);  
        }else{
            cap >> frame;
            //frame=imread("../aruco-tag-0.png", -1);  
            //frame=imread("../marker_scene_demo.jpg", -1);  
            if (frame.empty()) break;
        }

        //printf("img %4d,%4d\n",frame.rows,frame.cols);
        //printf("factor %12.6f %12.6f\n",frame.cols/1024.0,frame.rows/512.0);

		nb=doAruco(frame);

        imshow("Aruco", frame);

		//printf("time=%12.6f  %12.6fms   w=%d h=%d\n",now,(now-last)*1000,frame.size().width,frame.size().height);
		//last=now;
		char c;
        if(  imagesDir[0] ) c=waitKey(0);
        c=waitKey(videoFileName[0]?30:1);
        if( c==27 ) break;
        //if( nb==0 ) continue; // pas de tag...
        // calibrage
        if( c=='a' ) {
            if( nb>0 )  {
                printf("===============  ++++++\n");
                calibreAccumule(markerCorners[0]);
            }
        }else if( c=='c' ) {
            printf("=============== GO  =========\n");
            calibreDo();
        }else if( c=='s' ) {
            trajectoryDump();
        }
	}


}

