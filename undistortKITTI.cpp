#include "opencv2/objdetect.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <string>
#include <iostream>
#include <sys/stat.h>

using namespace std;
using namespace cv;

/**
cv::initUndistortRectifyMap(*m_camData, *m_distData, cv::Mat(), *m_camData, cv::Size(resolution.first, resolution.second), CV_32FC1, *m_undistMap1, *m_undistMap2);
}

//create undistorted image
cv::remap(srcImg, *m_undistortedImg, *m_undistMap1, *m_undistMap2, cv::INTER_LINEAR);

*/


void readKD(Mat &k,Mat &d,char *kitti,int cam) {
    char calib[200];
    sprintf(calib,"%s/calib_cam_to_cam.txt",kitti);
    FILE *F=fopen(calib,"r");
    if(  F==NULL  )  {  printf("ERR %s\n",calib);exit(0);  }

    k=Mat_<double>(3,3);
	d=Mat_<double>(1,5);

    char  line[500];
    int n;
    for(;;) {
        if( fscanf(F," %s:",line)!=1 ) break;
        //printf("Line >%s<\n",line);
        if( sscanf(line,"K_%02d:",&n)==1 && n==cam )  {
            for(int j=0;j<9;j++)  fscanf(F," %lf",((double *)k.data)+j);
        }
        if( sscanf(line,"D_%02d:",&n)==1 && n==cam )  {
            for(int j=0;j<5;j++)  fscanf(F," %lf",((double *)d.data)+j);
        }
        fscanf(F,"%[^\n]",line);
    }
    fclose(F);

    //k=(Mat_<double>(3,3) << 9.597910e+02, 0.000000e+00, 6.960217e+02, 0.000000e+00, 9.569251e+02, 2.241806e+02, 0.000000e+00, 0.000000e+00, 1.000000e+00);
	//d=(Mat_<double>(1,5) << -3.691481e-01, 1.968681e-01, 1.353473e-03, 5.677587e-04, -6.770705e-02);

}


int main(int argc, char *argv[])
{
char kitti[200];
char data[200];
int  cam;

strcpy(kitti,"/data/datasets/KITTI-RAW/2011_09_26");
strcpy(data,"2011_09_26_drive_0001_extract");
cam=2;

    for(int i=1;i<argc;i++) {
        if( strcmp(argv[i],"-h")==0 ) {
            printf("usage:  %s -kitti /data/datasets/KITTI-RAW/2011_09_26 -data 2011_09_26_drive_0001_extract  -cam 2\n",argv[0]);
            exit(0);
        }
        if( strcmp(argv[i],"-kitti")==0 && i+1<argc ) {
            strcpy(kitti,argv[i+1]);
            i++;continue;
        }
        if( strcmp(argv[i],"-data")==0 && i+1<argc ) {
            strcpy(data,argv[i+1]);
            i++;continue;
        }
        if( strcmp(argv[i],"-cam")==0 && i+1<argc ) {
            cam=atoi(argv[i+1]);
            i++;continue;
        }
        printf("arg %d %s\n",i,argv[i]);
    }

    printf("KITTI %s\n",kitti);
    printf("data  %s\n",data);
    printf("cam   %d\n",cam);


	Mat k;
	Mat dist;
    readKD(k,dist,kitti,cam);
    //k=(Mat_<double>(3,3) << 9.597910e+02, 0.000000e+00, 6.960217e+02, 0.000000e+00, 9.569251e+02, 2.241806e+02, 0.000000e+00, 0.000000e+00, 1.000000e+00);
	cout << k ;

	//dist=(Mat_<double>(1,5) << -3.691481e-01, 1.968681e-01, 1.353473e-03, 5.677587e-04, -6.770705e-02);
	cout << dist << "\n";


	Mat map1,map2;
    Mat newcam;

    char inbuf[500];
    sprintf(inbuf,"%s/%s/image_%02d/data/%010d.png",kitti,data,cam,0);
    char calbuf[500];
    sprintf(calbuf,"%s/calib_camera_%02d.txt",kitti,cam);

    Mat  im=imread(inbuf);
    //imshow("blub",im);
    //waitKey(0);

    cout << im.cols  << " "<<im.rows<<"\n";

    Mat rot=(Mat_<double>(3,3) << 1.,0.,0.,0.,1.,0.,0.,0.,1.);
    cout << rot  << "\n";

    Mat newk=getOptimalNewCameraMatrix(k,dist,
            cv::Size(im.cols,im.rows),
            0.,  // do not see all points
            cv::Size(1242,375),
            NULL,0); // do not center principal point

    cout << newk << "\n";

    // output  new K and size to  calib_cam_02.txt
    FILE *F=fopen(calbuf,"w");
    if( F!=NULL  )  {
        fprintf(F,"K_undist_%02d:",cam);
        for(int i=0;i<9;i++) fprintf(F," %.6e",newk.at<double>(i/3,i%3));
        fprintf(F,"\n");
        fprintf(F,"S_undist_%02d: %.6e %.6e\n",cam,1242.0,375.0);
        fclose(F);
    }

	initUndistortRectifyMap(k,dist, rot, newk, cv::Size(1242,375), CV_32FC1, map1,map2);


	imwrite("map1.png",map1);
	imwrite("map2.png",map2);


    char outbuf[500];
    sprintf(outbuf,"%s/%s/image_%02d/data-undist",kitti,data,cam);
    mkdir(outbuf,0755);

    Mat im0;
    for(int i=0;;i++)  {
        sprintf(inbuf,"%s/%s/image_%02d/data/%010d.png",kitti,data,cam,i);
        printf("read %s\n",inbuf);
        im=imread(inbuf);
        if( im.empty() ) break;
        remap(im,im0,map1,map2,INTER_LINEAR);
        //imshow("blub", im);
        //imshow("toto", im0);
        //waitKey(100);
        sprintf(outbuf,"%s/%s/image_%02d/data-undist/%010d.png",kitti,data,cam,i);
        imwrite(outbuf,im0);
        printf("write %s\n",outbuf);
    }
}









