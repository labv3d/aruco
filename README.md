# aruco

tracking de tag aruco.

## Installation

mkdir build
cd build
cmake ..
make

## Utilisation

./aruco [-video fichier.mp4]

On peut specifier un video, sinon ca sera  la  webcam 0

## Documentation

Aruco tags:  https://docs.opencv.org/4.x/d5/dae/tutorial_aruco_detection.html



## Tag

Pour generer un tag : http://chev.me/arucogen/

Exemple de tag:

![tag5x5_026.png](https://gitlab.com/labv3d/aruco/raw/master/tag5x5_026.png)

## Compilation/Installation

Tout d'abord,  installer opencv.

Sur Linux:
*sudo apt install  libopencv-dev libopencv-contrib-dev*


## Installing/compiling openCV

(generalement pas necessaire)

Install openCV

https://github.com/opencv/opencv/releases/tag/4.5.4

Direct source of openCV:  https://github.com/opencv/opencv/archive/refs/tags/4.5.4.tar.gz

### pour le openCV de base...

cd opencv-4.5.4
mkdir build
cd build
cmake ..
cd opencv
make -j8
sudo make install

### aruco est une "contrib"

https://github.com/opencv/opencv_contrib

Direct: https://github.com/opencv/opencv_contrib/archive/refs/heads/4.x.zip

# back to opencv
cd opencv-4.5.4/build
cmake -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-4.x/modules -DBUILD_opencv_legacy=OFF ..
make -j8
sudo make install




