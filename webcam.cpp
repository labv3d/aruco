#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgcodecs.hpp"
#include <string>
#include <iostream>
#include <sys/time.h>

using namespace std;


int main(int argc, char *argv[])
{
    cv::VideoCapture cap(0);
    if(!cap.isOpened())
    {
        cout << "Cannot open a camera" << endl;
        return -4;
    }

	//cap.set(CAP_PROP_FRAME_WIDTH,1920);
	//cap.set(CAP_PROP_FRAME_HEIGHT,1080);
	//cap.set(cv::CAP_PROP_FRAME_WIDTH,800);
	//cap.set(cv::CAP_PROP_FRAME_HEIGHT,600);
	//cap.set(CAP_PROP_AUTOFOCUS, 0);

    cv::Mat frame;

	for(int i=0;/*i<1*/;i++) {
        cap >> frame;
		//frame=imread("../aruco-tag-0.png", -1);  
		//frame=imread("../marker_scene_demo.jpg", -1);  
        if (frame.empty()) break;

        printf("img %4d,%4d\n",frame.rows,frame.cols);

        cv::imshow("Aruco", frame);

		char c=cv::waitKey(1);
        if( c==27 ) break;
	}

}

