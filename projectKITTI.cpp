#include "opencv2/objdetect.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <string>
#include <iostream>
#include <sys/stat.h>

//
// Fabrique des cartes de profondeur
// a partir de velodyne_points/world_points.bin
// et image_02/intern.txt extern.txt epoch.txt
//


using namespace std;
using namespace cv;

//  ne  pas  touverh  a la  structure!!!
typedef struct {
    double x,y,z,reflectance,epoch;
} worldpoint;

// world points
long int  nbWP;
worldpoint *WP; // [nbWP]

// K
Matx33d K; // 3x3  param interne
Matx33d Ki; // 3x3  param interne inverse

// nb d' images
int  nbI=-1;

// cam to world pour chaque image
double *c2wD; // [nbI*4*4] par rangee
 
Matx44d *c2w; // [nbI]
Matx44d *w2c; // [nbI]

//  epoch, timestamp pour chaque image
double *epoch; // [nbI]

// taille d'une  image
int  nbR,nbC;


//
// format : 64bit reals, each  point is {x,y,z,reflectance,epoch}
//
void  readWorld(char *name)  {
    printf("read world points %s\n",name);
    FILE *F=fopen(name,"rb");
    if( F==NULL ) exit(0);

    // taille du fichier??
    fseek(F,0,SEEK_END);
    long int len=ftell(F);
    rewind(F);

    nbWP=len/(5*8);
    printf("len=%ld nb=%ld\n",len,nbWP);
    WP=(worldpoint  *)malloc(nbWP*sizeof(worldpoint));
    long int k=fread(WP,sizeof(worldpoint),nbWP,F);
    printf("k=%ld\n",k);

    /*
    for(int i=0;i<10;i++) {
        printf("%12.6f %12.6f %12.6f %12.6f %12.6f\n",WP[i].x,WP[i].y,WP[i].z,WP[i].reflectance,WP[i].epoch);
    }
    */
    fclose(F);
}

//
// matrice K. 3x3 en format texte
//
void  readIntern(char *name)  {
    double k[9];
    printf("read intern %s\n",name);
    FILE  *F=fopen(name,"r");
    for(int i=0;i<9;i++)  {
        fscanf(F," %lf",k+i);
    }
    fclose(F);
    //for(int i=0;i<9;i++)  printf("%12.6f\n",K[i]);
    K=Matx33d(k);
    Ki=K.inv();
    cout<<K<<"\n"<<Ki<<"\n";
}

//
// un certain nombre de matrices 4x4, en texte
//
void  readExtern(char *name)  {
    printf("read extern %s\n",name);
    FILE *F=fopen(name,"r");
    // combien de matrices??
    int n;
    for(n=0;;n++) {
        double  d;
        if( fscanf(F," %lf",&d)!=1  )  break;
    }
    printf("N=%d %d\n",n,n/16);
    if( nbI>0 && nbI!=n/16  ) exit(0);
    nbI=n/16;

    c2wD=(double *)malloc(nbI*4*4*sizeof(double));
    c2w=(Matx44d *)malloc(nbI*sizeof(Matx44d));
    w2c=(Matx44d *)malloc(nbI*sizeof(Matx44d));

    rewind(F);
    for(int i=0;i<nbI*4*4;i++) fscanf(F," %lf",c2wD+i);

    for(int i=0;i<nbI;i++) {
        c2w[i]=Matx44d(c2wD+i*16);
        w2c[i]=c2w[i].inv();
    }

    /*
    for(int i=0;i<3;i++) {
        printf("---- %d ----\n",i);
        for(int r=0;r<4;r++) {
            for(int c=0;c<4;c++) {
                printf(" %12.6f",c2wD[i*16+r*4+c]);
            }
            printf("\n");
        }
    }
    */

    //cout <<c2w[0]<<"\n";
    //cout <<w2c[0]<<"\n";
    //cout <<(w2c[0]*c2w[0])<<"\n";

    fclose(F);
}

void  readEpoch(char *name)  {
    printf("read epoch %s\n",name);
    FILE *F=fopen(name,"r");
    int n;
    //  combien de timestamps?
    for(n=0;;n++) {
        double  d;
        if( fscanf(F," %lf",&d)!=1  )  break;
    }
    //printf("nbE=%d\n",n);

    if( nbI>0 && nbI!=n  ) exit(0);
    nbI=n;
    epoch=(double  *)malloc(nbI*sizeof(double));
    rewind(F);
    for(int i=0;i<nbI;i++)  {
        fscanf(F," %lf",epoch+i);
    }
    fclose(F);

    //for(int i=0;i<10;i++) printf("%12.6f\n",epoch[i]);
    printf("epoch[9]=%14.8f\n",epoch[9]);
}

//
// projete un  point x,y,z avec une camera et K
//
// si selected est <0 alors ajuste selected  a 0 ou 1 (1=visible)
//
// K . w2c . P3D
//
int proj(worldpoint p,int n,double &x,double &y,double &z,Matx44d plans)  {
    //printf("[%12.6f %12.6f %12.6f]\n",p.x,p.y,p.z);
    Matx41d p1(p.x,p.y,p.z,1.0);
    //cout<<"p1"<<p1<<"\n";
    //cout<<"w2c"<<w2c[n]<<"\n";

    //  on teste le  point par rapport  aux  plans
    Matx41d visi=plans*p1;
    //cout << "visi"<< visi<<"\n";
    if( visi(0)<=0. || visi(1)<=0. ||  visi(2)<=0.  ||  visi(3)<=0. ) {
        return(5);
    }

    Matx41d p2;
    p2=w2c[n]*p1;
    //cout<<"p2"<<p2<<"\n";
    //cout<<p2(0)<<"\n";
    //////if( p2(2)<=0.0 ) {printf("CUT\n");return(-1);} // en  arriere
    z=p2(2);
    Matx31d q2(p2(0),p2(1),p2(2));
    //cout<<"q2"<<q2<<"\n";
    q2=K*q2;
    //cout<<"q2"<<q2<<"\n";
    q2/=q2(2);
    //cout<<"q2"<<q2<<"\n";
    x=q2(0);
    y=q2(1);
    /////if( x<=0.0 || x>=nbC || y<=0 || y>=nbR  ) {printf("CUT\n");return(1);}
    return(0);
}

unsigned short clip(double v,double  vmin,double vmax) {
    double a=(v-vmin)/(vmax-vmin);
    if( a<0. ) return(0);
    if( a>1. ) return(65535);
    return(round(a*65535.));
}

    double zmin=999999999.0;
    double zmax=-999999999.0;

//
// reproj le monde dans l'image n
// tmin et tmax sont  les temps avant  et  apres  une  photo
//
void reprojSimple(int n,char *name,double  tmin,double tmax,Matx44d plans,const char *midfix) {
    printf("-- reproj %d --\n",n);
    char buf[600];

    // une depth map
    double  *depth; // [nbR*nbC]
    depth=(double *)malloc(nbR*nbC*sizeof(double));
    // une  time map
    double  *time; // [nbR*nbC]
    time=(double *)malloc(nbR*nbC*sizeof(double));

    for(int i=0;i<nbR*nbC;i++) depth[i]=999.0;
    for(int i=0;i<nbR*nbC;i++) time[i]=999.0;

    // ne tient pas compte  du  temps  pour  l' instant
    double x,y,z; // (x,y) en pixel, z dans la camera
    int xi,yi;
    double xf,yf;
    double t;

    int nbTry=0;
    int nbVote=0;

    /**
    printf("epoch from %14.8f\n",epoch[n]+tmin);
    printf("epoch from %14.8f\n",epoch[n]+tmax);
    printf("nbWP=%d\n",nbWP);
    printf("epoch %08d point  %14.8f\n",0,WP[0].epoch);
    int j;
    for(j=nbWP-1;j>0;j--) if( WP[j].epoch>10.0  )  break;
    for(int  i=0;i<200;i++) {
        printf("epoch %08d point  %14.8f\n",j-i,WP[j-i].epoch);
    }
    printf("epoch %08d point  %14.8f\n",nbWP-1,WP[nbWP-1].epoch);
    **/

    for(long int i=0;i<nbWP;i++) {
        if( i%10000000==0 ) printf("%ld\n",i);
        if( WP[i].epoch<epoch[n]+tmin ||  WP[i].epoch>epoch[n]+tmax ) continue; // outside time
        nbTry++;
        if( proj(WP[i],n,x,y,z,plans)==0 ) {
            if( z<zmin ) { zmin=z; printf("ZMIN %12.6f ZMAX %12.6f\n",zmin,zmax); }
            if( z>zmax ) { zmax=z; printf("ZMIN %12.6f ZMAX %12.6f\n",zmin,zmax); }
            t=abs(WP[i].epoch-epoch[n]);
            // binarize
            xi=floor(x);xf=x-xi;
            yi=floor(y);yf=y-yi;
            // check meme si ce n'est pas 100% necessaire
            if( xi<0 || yi<0 || xi>=nbC  || yi>=nbR ) continue;
            //printf("[%d+%f,%d+%f]\n",xi,xf,yi,yf);
            // vote!
            int j=yi*nbC+xi;
            nbVote++;
            if( z<depth[j] ) { depth[j]=z;time[j]=t; }
            //if( xi+1<nbC && z<depth[j+1] ) { depth[j+1]=z;time[j+1]=t; }
            //if( yi+1<nbR && z<depth[j+nbC] ) { depth[j+nbC]=z;time[j+nbC]=t; }
            //if( xi+1<nbC && yi+1<nbR && z<depth[j+nbC+1] ) { depth[j+nbC+1]=z;time[j+nbC+1]=t; }
            /*
            if( t<time[j] ) { depth[j]=z;time[j]=t; }
            if( xi+1<nbC && t<time[j+1] ) { depth[j+1]=z;time[j+1]=t; }
            if( yi+1<nbR && t<time[j+nbC] ) { depth[j+nbC]=z;time[j+nbC]=t; }
            if( xi+1<nbC && yi+1<nbR && t<time[j+nbC+1] ) {depth[j+nbC+1]=z;time[j+nbC+1]=t;}
            */
        }
    }

    printf("nb vote=%d   try=%d\n",nbVote,nbTry);

    Mat id(nbR,nbC,CV_16U);
    Mat id0(nbR,nbC,CV_16U);
    Mat id1(nbR,nbC,CV_16U);
    Mat id2(nbR,nbC,CV_16U);
    Mat id012(nbR,nbC,CV_16UC3);
    Mat idT(nbR,nbC,CV_16U);
    unsigned short *d=(unsigned short *)id.data;
    unsigned short *d0=(unsigned short *)id0.data;
    unsigned short *d1=(unsigned short *)id1.data;
    unsigned short *d2=(unsigned short *)id2.data;
    unsigned short *d012=(unsigned short *)id012.data;
    unsigned short *dT=(unsigned short *)idT.data;
    for(int i=0;i<nbR*nbC;i++) {
        //d[i]=clip(1.0/depth[i],0.,0.25);
        d[i]=clip(depth[i]/256.,0.,1.);
        if(d[i]==65535) d[i]=0;
        d0[i]=clip(depth[i],0.,20.);
        d1[i]=clip(depth[i],20.,50.);
        d2[i]=clip(depth[i],50.,120.);
        d012[i*3+2]=d0[i];
        d012[i*3+1]=d1[i];
        d012[i*3+0]=d2[i];
        dT[i]=clip(time[i],0.,(epoch[nbI-1]-epoch[0])/3);
    }

    sprintf(buf,"%s/depth-%s-%05d.png",name,midfix,n);
    printf("write %s\n",buf);
    imwrite(buf,id);
    /*
    sprintf(buf,"%s/depth-000-020-%08d.png",name,n);
    imwrite(buf,id0);
    sprintf(buf,"%s/depth-020-050-%08d.png",name,n);
    imwrite(buf,id1);
    sprintf(buf,"%s/depth-050-120-%08d.png",name,n);
    imwrite(buf,id2);
    sprintf(buf,"%s/time-%08d.png",name,n);
    imwrite(buf,idT);
    */

    //sprintf(buf,"%s/depth-rgb-%05d.png",name,n);
    //imwrite(buf,id012);

    imshow("blub", id);
    waitKey(50);

    free(depth);
    free(time);

}

//
// calcule  un  plan de crop dans  le monde
// pour un carre dans l' image d'une camera
//
Matx31d zap(double x,double y,double z,int  n) {
    Matx31d p(x,y,z);
    p=Ki*p;
    Matx41d q(p(0),p(1),p(2),1.0);
    q=c2w[n]*q;
    Matx31d s(q(0),q(1),q(2));
    return s;
}
Matx41d cross(Matx31d u,Matx31d v,Matx31d ref) {
    Matx31d q( u(1)*v(2)-u(2)*v(1),
               u(2)*v(0)-u(0)*v(2),
               u(0)*v(1)-u(1)*v(0));
    return Matx41d(q(0),q(1),q(2),-(q.t()*ref)(0));
}
Matx44d cropPlan(double xmin,double xmax,double  ymin,double ymax,int n)  {
    Matx31d p0=zap(0.,0.,0.,n);
    Matx31d pa=zap(xmin,ymin,1.,n);
    Matx31d pb=zap(xmin,ymax,1.,n);
    Matx31d pc=zap(xmax,ymax,1.,n);
    Matx31d pd=zap(xmax,ymin,1.,n);
    Matx41d u0=cross(pb-p0,pa-p0,p0);
    Matx41d u1=cross(pa-p0,pd-p0,p0);
    Matx41d u2=cross(pd-p0,pc-p0,p0);
    Matx41d u3=cross(pc-p0,pb-p0,p0);
    Matx44d plans;
    for(int i=0;i<4;i++) {
        plans(0,i)=u0(i);
        plans(1,i)=u1(i);
        plans(2,i)=u2(i);
        plans(3,i)=u3(i);
    }
    cout << plans <<"\n";
    return plans;
}


// prochaine image dans  une  liste  separee  par des ,
int getNextImg(char *img,int &imgPos) {
    // look for  next , or  0
    int i=imgPos;
    for(;img[i] && img[i]!=',';i++) ;
    if( img[imgPos]==0 ) return(-1); // done!!
    int  v;
    sscanf(img+imgPos," %d ,",&v);
    printf("now i=%d imgPos=%d v=%d\n",i,imgPos,v);
    imgPos=i+1;
    return(v);
}


int main(int argc, char *argv[])
{
char kitti[200];
char data[200];
int  cam;
char img[300];
int  imgPos=0; // where  in  the string, for  next image

strcpy(kitti,"/data/datasets/KITTI-RAW/2011_09_26");
strcpy(kitti,"/media/roys/BACKUP/KITTI-RAW-SPHERICAL");
strcpy(data,"2011_09_26_drive_0002_extract");
img[0]=0;  // all  images
cam=2;

    for(int i=1;i<argc;i++) {
        if( strcmp(argv[i],"-h")==0 ) {
            printf("usage:  %s -kitti %s -data %s  -cam %d -img ""\n",argv[0],kitti,data,cam);
            exit(0);
        }
        if( strcmp(argv[i],"-kitti")==0 && i+1<argc ) {
            strcpy(kitti,argv[i+1]);
            i++;continue;
        }
        if( strcmp(argv[i],"-data")==0 && i+1<argc ) {
            strcpy(data,argv[i+1]);
            i++;continue;
        }
        if( strcmp(argv[i],"-cam")==0 && i+1<argc ) {
            cam=atoi(argv[i+1]);
            i++;continue;
        }
        if( strcmp(argv[i],"-img")==0 && i+1<argc ) {
            strcpy(img,argv[i+1]);
            i++;continue;
        }
        printf("arg %d %s\n",i,argv[i]);
    }

    printf("KITTI %s\n",kitti);
    printf("data  %s\n",data);
    printf("cam   %d\n",cam);
    printf("img   %s\n",img);

    /*
    for(;;)  {
        int n=getNextImg(img,imgPos);
        if( n<0 )  break;
        printf("n=%d\n",n);
    }
    exit(0);
    */



    char buf[500];
    sprintf(buf,"%s/%s/velodyne_points/world_points.bin",kitti,data);
    readWorld(buf);

    sprintf(buf,"%s/%s/image_%02d/intern.txt",kitti,data,cam);
    readIntern(buf);

    sprintf(buf,"%s/%s/image_%02d/extern.txt",kitti,data,cam);
    readExtern(buf);

    sprintf(buf,"%s/%s/image_%02d/epoch.txt",kitti,data,cam);
    readEpoch(buf);

    // on doit  lire une image pour avoir la dimension
    sprintf(buf,"%s/%s/image_%02d/data-undist/%010d.png",kitti,data,cam,0);
    Mat im=imread(buf);
    nbR=im.rows;
    nbC=im.cols;
    printf("Image size (%d,%d)\n",nbC,nbR);

    //
    //  plans
    //

    //cout  <<  plans<<"\n";


    ///
    /// GO!
    ///
    char outbuf[500];
    sprintf(outbuf,"%s/%s/image_%02d/depth",kitti,data,cam);
    mkdir(outbuf,0755);

    // ramasse l'image
    //sprintf(buf,"%s/%s/image_%02d/data-undist/%010d.png",kitti,data,cam,0);
    //im=imread(buf);

    // on va reprojeter tout du point de vue d'une cam
    for(int i=0;i<nbI;i++) {
        int n; //  image a traiter

        n=getNextImg(img,imgPos);
        printf("---->n=%d\n",n);
        if( n<0 ) {
            if( img[0]==0 ) n=i;
            else break;
        }

        printf("---- %d -----\n",n);

        //Matx44d plans=cropPlan(300.-100.,300+100.,180-100.,180+100.,i);
        Matx44d plans=cropPlan(0.,nbC,0.,nbR,n);
        //reprojSimple(n,outbuf,-0.5,0.5,plans,"T5");
        //reprojSimple(n,outbuf,-0.1,0.1,plans,"T1");
        reprojSimple(n,outbuf,-2.5,2.5,plans,"T25");
    }


#ifdef SKIP
        //imshow("blub", im);
        //waitKey(100);
#endif
}









