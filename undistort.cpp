#include "opencv2/objdetect.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <string>
#include <iostream>

using namespace std;
using namespace cv;

/**
cv::initUndistortRectifyMap(*m_camData, *m_distData, cv::Mat(), *m_camData, cv::Size(resolution.first, resolution.second), CV_32FC1, *m_undistMap1, *m_undistMap2);
}

//create undistorted image
cv::remap(srcImg, *m_undistortedImg, *m_undistMap1, *m_undistMap2, cv::INTER_LINEAR);

*/


int main(int argc, char *argv[])
{

	Mat k;
    k=(Mat_<double>(3,3) << 9.597910e+02, 0.000000e+00, 6.960217e+02, 0.000000e+00, 9.569251e+02, 2.241806e+02, 0.000000e+00, 0.000000e+00, 1.000000e+00);
	cout << k ;

	Mat dist=(Mat_<double>(1,5) << -3.691481e-01, 1.968681e-01, 1.353473e-03, 5.677587e-04, -6.770705e-02);
	cout << dist << "\n";

	Mat map1,map2;
    Mat newcam;

    Mat  im=imread("../0000000000.png");
    //imshow("blub",im);
    //waitKey(0);

    cout << im.cols  << " "<<im.rows<<"\n";

	initUndistortRectifyMap(k,dist, Mat(), newcam, cv::Size(im.cols,im.rows), CV_32FC1, map1,map2);


	imwrite("map1.png",map1);
	imwrite("map2.png",map2);

    char buf[200];
    Mat im0;
    for(int i=0;i<114;i++)  {
        sprintf(buf,"/data/datasets/KITTI-RAW/2011_09_26/2011_09_26_drive_0001_extract/image_02/data/%010d.png",i);
        printf("read %s\n",buf);
        im=imread(buf);
        remap(im,im0,map1,map2,INTER_LINEAR);
        //imshow("blub", im);
        //imshow("toto", im0);
        //waitKey(100);
        sprintf(buf,"/data/datasets/KITTI-RAW/2011_09_26/2011_09_26_drive_0001_extract/image_02/data-undist/%010d.png",i);
        imwrite(buf,im0);
        printf("write %s\n",buf);
    }

}







#ifdef ORIGINAL

int main(int argc, char *argv[])
{

	FileStorage fs;
	fs.open("out_camera_data.yml", FileStorage::READ);
	Mat k;
	fs["camera_matrix"] >> k;
	cout << k ;

	Mat dist;
	fs["distortion_coefficients"] >> dist;
	cout << dist << "\n";

	Mat map1,map2;

	initUndistortRectifyMap(k,dist, Mat(), k, cv::Size(800,600), CV_32FC1, map1,map2);

	imwrite("map1.png",map1);
	imwrite("map2.png",map2);



    VideoCapture cap(2);
    if(!cap.isOpened())
    {
        cout << "Cannot open a camera" << endl;
        return -4;
    }

	cap.set(CAP_PROP_FRAME_WIDTH,800);
	cap.set(CAP_PROP_FRAME_HEIGHT,600);
	cap.set(CAP_PROP_AUTOFOCUS, 0);

	Mat frame;
	Mat img;
	
	for(;;) {
        cap >> frame;
        if (frame.empty()) break;

		remap(frame,img,map1,map2,INTER_LINEAR);

        imshow("blub", frame);
        imshow("toto", img);
		waitKey(1);
	}

}


#endif





